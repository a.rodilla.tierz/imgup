const ImageModel = require('./../infrastructure/models/ImageModel.js');

const ManageUploadImg = class {
  constructor(clientMongo) {
    this.clientMongo = clientMongo;
  }

  async saveImagesOnDB(file, user) {
    const workImgObj = {
      name: file.photos.name,
      type: file.photos.mimetype,
      size: file.photos.size,
      value: file.photos.data,
      md5: file.photos.md5,
      encoding: file.photos.encoding,
      owner: user.innerId,
    };
    return this.procesateInsertResponse(await this.clientMongo.insertInCollection('imagesUser', [workImgObj]));
  }

  procesateInsertResponse(responseInsert) {
    if (responseInsert.result.ok > 0) {
      return responseInsert.ops[0];
    } else {
      return null;
    }
  }

  async getUserImages(user) {
    const workImgObj = {
      owner: user.innerId,
    };
    const listImages = await this.clientMongo.queryManyToCollection('imagesUser', workImgObj);
    return listImages.map(img => {
      return new ImageModel(img);
    });
  }
}

module.exports = ManageUploadImg;
