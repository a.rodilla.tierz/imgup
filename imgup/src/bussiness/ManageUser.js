const UserUploader = require('./../infrastructure/models/UserUploader.js');

const ManageUser = class {
  constructor(clientMongo) {
    this.clientMongo = clientMongo;
  }

  async loadUser(userParams) {
    const queryObj = {
      name: userParams.name,
      password: userParams.password
    };
    return new UserUploader(await this.clientMongo.queryToCollection('listUsers', queryObj));
  }

  async createUser(userParams) {
    const queryObj = {
      name: userParams.name,
      password: userParams.password
    };
    const newUser = await this.clientMongo.insertInCollection('listUsers', [queryObj]);
    return new UserUploader(newUser.ops[0]);
  }
}

module.exports = ManageUser;
