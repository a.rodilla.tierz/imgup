const dotenv = require('dotenv');
const port = 3001;
// Set ready the server
const app = require('./app');

const httpClient = require('request-promise-native');
app.set('httpClient', httpClient);
dotenv.config({path: '.env'});
const server = app.listen((process.env['SERVER_PORT'] || port), () => {
  console.log(
    '  App is running at http://localhost:%d in %s mode'
  );
  console.log('  Press CTRL-C to stop\n');
});
