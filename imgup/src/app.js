const express = require('express');
const bodyParser = require("body-parser");
const http = require('http');
const dotenv = require('dotenv');
const mongopool = require('./infrastructure/dbpool/mongopool.js');
const factory = require('./services/RouterFactory.js');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const morgan = require('morgan');
const _ = require('lodash');

const app = express();
const router = express.Router();

dotenv.config({path: '.env'});
app.set('clientMongo', new mongopool());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(fileUpload({
    createParentPath: true
}));

app.use(cors());
app.use(morgan('dev'));
app.use('/', factory.RouterFactory(router));

module.exports = app;
