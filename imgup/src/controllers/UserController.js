const ManageUser = require('./../bussiness/ManageUser.js');
const ManageUploadImg = require('./../bussiness/ManageUploadImg.js');

exports.LoginUserValid = async (clientMongo, paramsUser) => {
  const comandObject = new ManageUser(clientMongo);
  const userCheck = await comandObject.loadUser(paramsUser);
  if (!userCheck || !userCheck.innerId) {
    return await comandObject.createUser(paramsUser);
  }
  return userCheck;
};

exports.GetUserImages = async (clientMongo, paramsUser) => {
  const comandObject = new ManageUser(clientMongo);
  const imagesObject = new ManageUploadImg(clientMongo);
  const userCheck = await comandObject.loadUser(paramsUser);
  if (userCheck && userCheck.innerId) {
    return await imagesObject.getUserImages(userCheck);
  }
  return [];
};
