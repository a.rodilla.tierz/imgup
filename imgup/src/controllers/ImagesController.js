const ManageUploadImg = require('./../bussiness/ManageUploadImg.js');
const ManageUser = require('./../bussiness/ManageUser.js');

exports.UpdateImage = async (clientMongo, paramsImg, userdatas) => {
  const comandObject = new ManageUploadImg(clientMongo);
  const userObject = new ManageUser(clientMongo);
  const userCheck = await userObject.loadUser(userdatas);
  if (userCheck && userCheck.innerId) {
    return await comandObject.saveImagesOnDB(paramsImg, userCheck);
  }
  return null;
};
