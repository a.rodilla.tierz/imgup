const ImageModel = class {
  constructor(dbObject) {
    if (dbObject) {
      this.innerId = dbObject._id || undefined;
      this.name = dbObject.name || undefined;
      this.type = dbObject.type || undefined;
      this.size = dbObject.size || undefined;
      this.value = dbObject.value || undefined;
      this.md5 = dbObject.md5 || undefined;
      this.encoding = dbObject.encoding || undefined;
      this.owner = dbObject.owner || undefined;
    }
  }
}

module.exports = ImageModel;
