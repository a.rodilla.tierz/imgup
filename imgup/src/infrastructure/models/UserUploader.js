const UserUploader = class {
  constructor(dbObject) {
    if (dbObject) {
      this.innerId = dbObject._id || undefined;
      this.name = dbObject.name || undefined;
      this.password = dbObject.password || undefined;
    }
  }
}

module.exports = UserUploader;
