const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

const MongoManager = class {
  constructor() {
    this.workDBO = undefined;
    const workUri = 'mongodb://' + process.env['MONGO_HOST'] + ':' + process.env['MONGO_PORT'] + '/'
    this.clientMongo = new MongoClient(workUri);
    this.getCollectionConect().then(database => {
      console.error('database in constructor');
      this.workDBO = database;
    });
  }

  async conectorMongo(collectionWork) {
    this.workDBO = await this.getCollectionConect();
    await this.creatorCollection(collectionWork);
  }

  async fileManagerIniter() {
    if (!this.workDBO) {
      await this.conectorMongo('files');
    }
  }

  async getCollectionConect() {
    return new Promise((resolve, reject) => {
      this.clientMongo.connect(function(err, db) {
        console.error('init mongodb');
        if (err) {
          console.error('infoError: ', err);
          reject(err);
        } else {
          resolve(db.db("mydb"));
        }
      });
    });
  }

  async creatorCollection(collectionName) {
    await this.workDBO.createCollection(collectionName, function(err, res) {
      if (err) throw err;
      console.log("Collection created!");
    });
  }

  async removeCollection(collection, workObj) {
    if (!this.workDBO) {
      await this.conectorMongo(collection);
    }
    const collectionWork = this.workDBO.collection(collection);
    return await collectionWork.remove(workObj);
  }

  async insertInCollection(collection, workObj)  {
    if (!this.workDBO) {
      await this.conectorMongo(collection);
    }
    return await this.workDBO.collection(collection).insertMany(workObj);
  }

  async queryToCollection(collection, query)  {
    if (!this.workDBO) {
      await this.conectorMongo(collection);
    }
    const collectionWork = this.workDBO.collection(collection);
    return await collectionWork.findOne(query);
  }

  async queryManyToCollection(collection, query)  {
    if (!this.workDBO) {
      await this.conectorMongo(collection);
    }
    const collectionWork = this.workDBO.collection(collection);
    return await collectionWork.find(query).toArray();
  }

  async counterCollection(collection, query)  {
    if (!this.workDBO) {
      await this.conectorMongo(collection);
    }
    const collectionWork = this.workDBO.collection(collection);
    return await collectionWork.countDocuments(query);
  }
}

module.exports = MongoManager;
