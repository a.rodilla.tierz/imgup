const ImagesController = require('./../controllers/ImagesController.js');
const UserController = require('./../controllers/UserController.js');

exports.updateImg = async (req, res) => {
  res.status(200).send({
    status: 'OK',
    succes: await ImagesController.UpdateImage(req.app.get('clientMongo'), req.files, req.body)
  });
};

exports.loginUser = async (req, res) => {
  res.status(200).send({
    status: 'OK',
    user: await UserController.LoginUserValid(req.app.get('clientMongo'), req.body)
  });
};

exports.userImages = async (req, res) => {
  res.status(200).send({
    status: 'OK',
    images: await UserController.GetUserImages(req.app.get('clientMongo'), req.query)
  });
};
