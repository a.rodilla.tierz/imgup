const apiController = require('./api.js');
const serviceMidddleware = require('./serviceMidddleware.js');

exports.RouterFactory = (router) => {
  router.use(serviceMidddleware.easyVerify);
  router.post('/uploadImg', apiController.updateImg);
  router.post('/loginUser', apiController.loginUser);
  router.get('/userImages', apiController.userImages);
  return router;
};
